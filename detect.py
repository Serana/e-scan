# import the necessary packages
import os

os.environ['KERAS_BACKEND'] = 'theano'
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import cv2
from PIL import Image
import numpy as np
from queue import Queue
from skimage import img_as_ubyte
from keras.preprocessing import image
from keras.models import model_from_json
import random
from skimage import io


COLOR = 0


def bfs(shapeMask, mask, x, y):
    height = len(shapeMask)
    width = len(shapeMask[0])

    dx = (1, 1, 0, -1, -1, -1, 0, 1, 5, 10, 15, -5, -10, -15, 0, 0, 0, 0, 0, 0)
    dy = (0, -1, -1, -1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 5, 10, 15, -5, -10, -15)
    sz = 0
    xmax = 0
    xmin = 1e9
    ymax = 0
    ymin = 1e9
    q = Queue()
    q.put((x, y))
    while (not q.empty()):
        (x, y) = q.get()
        ymax = max(ymax, y)
        ymin = min(ymin, y)
        xmax = max(xmax, x)
        xmin = min(xmin, x)
        mask[x][y] = True
        sz += 1
        for i in range(8):
            nx = x + dx[i]
            ny = y + dy[i]
            if (nx >= 0 and nx < height and ny >= 0 and ny < width):
                if (mask[nx][ny] == False and shapeMask[nx][ny] == COLOR):
                    mask[nx][ny] = True
                    q.put((nx, ny))
    return (sz, xmin, xmax, ymin, ymax)

def resize_to_X(img, x = 28.0):
    height = len(img)
    resized = cv2.resize(img, (0,0), fx=x/height, fy=x/height)
    return resized

def cut_and_resize_images(img, count = 6, w = 28, h = 28):
    width = len(img[0])
    height = len(img)
    imgs = []
    newW = width // count # / -> // 3 / 2 == 1.5 3 / 2 == 1
    for i in range(count):
        newImg = img[ 0 : height, newW * i : newW * (i + 1)]
        #print(newW * i , newW * (i + 1), 0 , height, width)
        resized = cv2.resize(newImg, (w, h))
        imgs.append(resized)
    return imgs


def findCounter(id, im, resc = 1, level = 140 ):
    nparr = np.fromstring(im, np.uint8)
    im = cv2.imdecode(nparr, cv2.IMREAD_COLOR)  # cv2.IMREAD_COLOR in OpenCV 3.1

    img = np.array(im)
    #img = cv2.convertFp16(img)
    #img = cv2.cvtColor(img_as_ubyte(img), cv2.COLOR_RGB2GRAY)
    #img = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
    img = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
    resized = cv2.resize(img, (0, 0), fx=resc, fy=resc)
    #lower = np.array([0])
    #upper = np.array([level])
    #shapeMask = cv2.inRange(resized, lower, upper)


    #blurred = cv2.GaussianBlur(resized, (5, 5), 0)
    blurred = resized
    shapeMask = cv2.threshold(blurred, 99, 255, cv2.THRESH_BINARY)[1]
    #print(shapeMask)
    crop_img2 = shapeMask
    crop_img2 = Image.fromarray(np.uint8(shapeMask))
    crop_img2.save("ImagesC/" + str(id) + "-1.jpg")

    height = len(shapeMask)
    width = len(shapeMask[0])

    mask = [[False for x in range(width)] for y in range(height)]

    ans = []
    for x in range(height):
        for y in range(width):
            if (mask[x][y] == False and shapeMask[x][y] == COLOR):
                ans.append(bfs(shapeMask, mask, x, y))
                #print("wee")
    ans.sort(reverse=True)
    print(id, ans)

    p1 = -1
    for t in range(0, len(ans)):
        h = ans[t][2] - ans[t][1]
        w = ans[t][4] - ans[t][3]
        if (h == 0 or w == 0):
            continue
        soot = w / h
        if not (2 <= soot and soot <= 6):
            print("OOPS soot", h, w, ans[t], soot)
            continue
        area_black = ans[t][0] / (h * w)
        if not (0.45 <= area_black and area_black <= 0.95):
            print("OOPS area", h, w, ans[t], soot)
            continue
        p1 = t
        break

    print(ans)
    crop_img_np = img[ans[p1][1] * 2:ans[p1][2] * 2, ans[p1][3] * 2:ans[p1][4] * 2]
    crop_img_np = resize_to_X(crop_img_np)
    crop_img = Image.fromarray(np.uint8(crop_img_np))
    crop_img.save("ImagesB/" + str(id) + ".jpg")
    return crop_img_np


def recognize(img, model):
    img = image.img_to_array(img)
    img = img.astype('float32')
    img /= 255
    img = img.reshape(-1, 28, 28, 1)
    classes = model.predict_classes(img, batch_size=10)
    return classes[0]

json_string = open('second_try.json', 'r').read()
model = model_from_json(json_string)
model.load_weights('second_try.h5')


def GetAnswer(img):
    id = random.randint(0, 1e50)
    counter = 1
    counter = findCounter(id, img, 0.5)
    imgs = cut_and_resize_images(counter)
    ans = []
    for j in range(6):
        crop_img = Image.fromarray(np.uint8(imgs[j]))
        path = "ImagesA/" + str(id) + "-" + str(j) + ".jpg"
        crop_img.save(path)
        ans.append(recognize(crop_img, model))
    print(ans)

    nparr = np.fromstring(img, np.uint8)
    im = cv2.imdecode(nparr, cv2.IMREAD_COLOR)  # cv2.IMREAD_COLOR in OpenCV 3.1
    img = np.array(im)
    Image.fromarray(np.uint8(img)).save("Images0/" + str(id) + "-" + str(ans) + ".jpg")


    return (ans, id, imgs)















