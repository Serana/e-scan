import datetime
import table
import const


months = {1: 'B', 2: 'C', 3: 'D', 4: 'E',
          5: 'F', 6: 'G', 7: 'H', 8: 'I',
          9: 'J', 10: 'K', 11: 'L', 12: 'M'}


def Account(UserID):
    id = table.UsersList().index(UserID) + 2
    return table.reg['C{0}'.format(id)].value


def Name(UserID):
    y = table.UsersList().index(UserID) + 2
    account = table.reg['D{0}'.format(y)].value
    t = table.Accounts().index((account)) + 2
    return table.readings['O{0}'.format(t)].value


def LastName(UserID):
    y = table.UsersList().index(UserID) + 2
    account = table.reg['E{0}'.format(y)].value
    t = table.Accounts().index((account)) + 2
    return table.readings['P{0}'.format(t)].value


def Date():
    day = datetime.datetime.today().day
    month = datetime.datetime.today().month
    year = datetime.datetime.today().year

    return '{0}/{1}/{2}'.format(day, month, year)


def CounterMode(UserID):
    cell = (table.users(UserID)[1].index(UserID)*2)+2
    print('zashel pop',cell)

    if (table.readings['N{0}'.format(cell)].value) == '1':
        print('table',table.readings['N{0}'.format(cell)].value)
        return '1'
    elif (table.readings['N{0}'.format(cell)].value) == '2':
        print('table2',table.readings['N{0}'.format(cell)].value)
        return '2'


def CellData(UserID):
    return table.readings['N{0}'.format(table.AccountsList().index(int(table.reg['C{0}'.format(
        table.users(UserID)[1].index(UserID) + 2)].value)) + 2)].value


def TwoCounterData(cellA):
    day_now = table.readings['{0}{1}'.format(
        months.get(datetime.datetime.today().month),
        cellA + 2)].value
    night_now = table.readings['{0}{1}'.format(
                months.get(datetime.datetime.today().month),
                cellA + 3)].value
    day_past = table.readings['{0}{1}'.format(
        months.get(datetime.datetime.today().month - 1),
        cellA + 2)].value
    night_past = table.readings['{0}{1}'.format(
                months.get(datetime.datetime.today().month - 1),
                cellA + 3)].value

    day_rate = 2.5
    night_rate = 1.5

    money = day_rate * day_now + night_rate * night_now

    text = const.account2Data.format(day_now, night_now, day_past, night_past,
                                     money, day_rate, night_rate)

    return text


ucompanies = {'UK_1': 'Управляющая компания «Лифт»',
              'UK_4': 'Управляющая компания «В»',
              'UK_2': 'Управляющая компания «Будущее»',
              'UK_3': 'Управляющая компания «ПреактумЪ»'}
