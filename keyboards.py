import telebot
import const
from telebot import types


bot = telebot.TeleBot(const.token)


# Клавиатуры
mainKeyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                         one_time_keyboard=True, row_width=2)
sendDataButton = types.KeyboardButton(const.sendData)
notificationButton = types.KeyboardButton(const.setNotify)
donateButton = types.KeyboardButton(const.donate)
mainKeyboard.add(sendDataButton)
mainKeyboard.add(notificationButton, donateButton)

# Методы отправки данных
dataMethods = types.InlineKeyboardMarkup(row_width=1)
byPhoto = types.InlineKeyboardButton(text='\U0001F4F7 Отправить фотографию',
                                     callback_data='sendPhoto')
byHandSending = types.InlineKeyboardButton(text='\U0001F4F2 Ввести данные вручную',
                                           callback_data='sendingByHand')
dataMethods.add(byPhoto, byHandSending)


# Инлайн
sendMail = types.InlineKeyboardMarkup(row_width=1)
send = types.InlineKeyboardButton(text='\U0001F3E0 Отправить данные в УК',
                                  callback_data='SendDataMail')
edit = types.InlineKeyboardButton(text='\U0001F4DD Изменить данные',
                                  callback_data='EditData')
sendMail.add(send, edit)


companiesKeyboard = types.InlineKeyboardMarkup(row_width=1)
uk1 = types.InlineKeyboardButton(text='Управляющая компания «Лифт»', callback_data='UK_1')
uk4 = types.InlineKeyboardButton(text='Управляющая компания «В»', callback_data='UK_4')
uk2 = types.InlineKeyboardButton(text='Управляющая компания «Будущее»', callback_data='UK_2')
uk3 = types.InlineKeyboardButton(text='Управляющая компания «ПреактумЪ»', callback_data='UK_3')
companiesKeyboard.add(uk1, uk4, uk2, uk3)


editingData = types.InlineKeyboardMarkup(row_width=1)
byHand = types.InlineKeyboardButton(text='\U0001F4F2 Ввести вручную',
                                    callback_data='byHand')
photoAgain = types.InlineKeyboardButton(text='\U0001F4F7 Отправить фото повторно',
                                        callback_data='photoAgain')
editingData.add(byHand, photoAgain)


notifyKeyboard = types.InlineKeyboardMarkup(row_width=1)
setNotifyButton = types.InlineKeyboardButton(text='\U0001F514 Настроить уведомления',
                                             callback_data='setNotify')
noSetNotify = types.InlineKeyboardButton(text='\u274C Отменить', callback_data='cancel')
notifyKeyboard.add(setNotifyButton, noSetNotify)


noNotifyKeyboard = types.InlineKeyboardMarkup(row_width=1)
offNotify = types.InlineKeyboardButton(text='\U0001F515 Отключить уведомления',
                                       callback_data='offNotify')
setNotifyButton = types.InlineKeyboardButton(text='\U0001F4C6 Изменить дату',
                                             callback_data='setNotify')
noSetNotify = types.InlineKeyboardButton(text='\u274C Отменить', callback_data='cancel')
noNotifyKeyboard.add(offNotify, setNotifyButton, noSetNotify)

donateURL = types.InlineKeyboardMarkup()
donateURLButton = types.InlineKeyboardButton(text='\U0001F4B8 Поддержать проект',
                                             url='https://yasobe.ru/na/escan_bot')
donateURL.add(donateURLButton)

rateTime = types.InlineKeyboardMarkup(row_width=1)
oneRate = types.InlineKeyboardButton(text='Однотарифный', callback_data='1_rate')
twoRate = types.InlineKeyboardButton(text='Двухтарифный', callback_data='2_rate')
rateTime.add(oneRate, twoRate)
