import smtplib
from email.mime.text import MIMEText
from email.header import Header
import const
import table
import functions
import datetime


def SendMail(UserID):
    cell = table.users(UserID)[1].index(UserID) + 2
    cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)

    # Настройки
    mail_sender = const.postMail
    mail_receiver = 'fakeucompany@gmail.com'
    username = const.postMail
    password = 'escanbot123'
    server = smtplib.SMTP('smtp.gmail.com:587')

    # Формируем тело письма
    subject = 'Показания счётчика электроэнергии'

    def Body():
        if functions.CounterMode(UserID) == '1':
            body = const.mailText_1.format(
                datetime.datetime.today().day,
                datetime.datetime.today().month,
                datetime.datetime.today().year,
                table.reg['C{0}'.format(cell)].value,
                str(table.readings['{0}{1}'.format(functions.months.get(
                    datetime.datetime.today().month), cellA + 2)].value))
        else:
            body = const.mailText_2.format(
                datetime.datetime.today().day,
                datetime.datetime.today().month,
                datetime.datetime.today().year,
                table.reg['C{0}'.format(cell)].value,
                table.readings['{0}{1}'.format(functions.months.get(
                    datetime.datetime.today().month), cellA + 2)].value,
                table.readings['{0}{1}'.format(functions.months.get(
                    datetime.datetime.today().month), cellA + 3)].value)
        return body

    msg = MIMEText(Body(), 'plain', 'utf-8')
    msg['Subject'] = Header(subject, 'utf-8')

    # Отпавляем письмо
    server.starttls()
    server.ehlo()
    server.login(username, password)
    server.sendmail(mail_sender, mail_receiver, msg.as_string())
    server.quit()
