import telebot
import const
import table
import threading
import datetime
import functions

bot = telebot.TeleBot(const.token)


def Status(UserID):
    cell = table.users(UserID)[1].index(UserID) + 2
    if str(table.notify['B{0}'.format(cell)].value) == 'None' or \
       str(table.notify['B{0}'.format(cell)].value) == '':
        return const.noNotify
    else:
        return const.yesNotify


def TodayNotify():
    """ Сегодня сдают показания: """
    today = datetime.datetime.today().day
    notifyList = table.notifyList
    notifyList.append('the last element')
    last = notifyList.index('the last element')
    todayNotify = []
    i = 0
    while i <= last:
        if str(today) == notifyList[i]:
            todayNotify.append(table.notify['A{0}'.format(i + 2)].value)
            i += 1
        else:
            i += 1
    notifyList.remove('the last element')

    return todayNotify


def SendNotify():
    """ Отправка уведомлений """
    print('Sending')
    List = TodayNotify()
    try:
        l = 0
        while l <= List.index(List[-1]):
            bot.send_message(List[l], const.notification.format(functions.Date()),
                             parse_mode='HTML')
            cell = table.users(List[l])[1].index(List[l]) + 2
            table.notify['C{0}'.format(cell)] = 'Отправлено'
            table.saving.save(table.Table)
            l += 1

        otherList = table.notifyList
        t = 0
        while t <= List.index(List[-1]):
            otherList.remove(List[t])
            t += 1
        b = 0
        while b <= otherList.index(otherList[-1]):
            cell = table.users(List[b])[1].index(List[b]) + 2
            table.notify['C{0}'.format(cell)] = ''
            table.saving.save(table.Table)
            b += 1

    except IndexError:
        pass


def RememberSending(List):
    """ Отправка уведомлений """
    try:
        l = 0
        while l <= List.index(List[-1]):
            bot.send_message(List[l], const.notification.format(functions.Date()),
                             parse_mode='HTML')
            cell = table.users(List[l])[1].index(List[l]) + 2
            table.notify['C{0}'.format(cell)] = 'Отправлено'
            table.saving.save(table.Table)
            l += 1
    except IndexError:
        pass


def CheckingNotify():
    """ Проверка уведомлений """
    goTime = 10  # Час рассылки уведомлений
    time_ = datetime.datetime.today().hour

    remember = []

    if time_ >= goTime:
        today = []
        today.extend(TodayNotify())
        a = 0
        today.append('the last element')
        last = today.index('the last element')
        while a < last:
            cell = table.users(today[a])[1].index(today[a]) + 2
            if table.notify['C{0}'.format(cell)].value == 'Отправлено':
                a += 1
            elif table.notify['C{0}'.format(cell)].value == 'Уведомления отключены':
                a += 1
            else:
                remember.append(today[a])
                a += 1
        RememberSending(remember)
    else:
        m = int(str(datetime.datetime.today().minute))
        h = 60 - int('{0}'.format(m))
        hrs = goTime - 1 - datetime.datetime.today().hour
        minutes = h * 60
        hours = hrs * 3600
        timer = minutes + hours
        t = threading.Timer(timer, SendNotify)
        t.start()
