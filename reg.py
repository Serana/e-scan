import telebot
import const
import table
import datetime
import keyboards
import functions


bot = telebot.TeleBot(const.token)

keys_ = {'1': ['B', '2', const.city], '2': ['C', '3', const.street],
         '3': ['D', '4', const.house], '4': ['E', '5', const.flat],
         '5': ['F', '6', const.mail], '6': ['G', '7', const.account],
         '7': ['H', '8', const.index], '8': ['I', '9', const.rateTime],
         '9': ['J', '10', const.rateTime], '10': ['K', '11', const.rate],
         '11': ['L', '12', '2 тариф'], '12': ['M', '13', '3 тариф'],
         '13': ['J', '10', const.lastMonth], '14': ['K', '11', '2 показания'],
         '15': ['L', '12', '3 показания']}


def reg(message, key):
    cell = table.users(message.from_user.id)[1].index(message.from_user.id) + 2

    if key == '8':
        table.reg['{0}{1}'.format(keys_.get(key)[0], cell)] = message.text
        table.reg['M{0}'.format(cell)] = keys_.get(key)[1]
        table.saving.save(table.Table)
        bot.send_message(message.from_user.id, keys_.get(key)[2],
                         reply_markup=keyboards.rateTime, parse_mode='HTML')
    elif key == '9':
        table.readings['{0}{1}'.format(functions.months.get(
            datetime.datetime.today().month - 1), cell + 2)] = message.text
        table.reg['M{0}'.format(cell)] = keys_.get(key)[1]
        table.saving.save(table.Table)
        bot.send_message(message.from_user.id, keys_.get(key)[2], parse_mode='HTML')
    elif key == '10':
        table.reg['K{0}'.format(cell)] = message.text
        if table.reg['J{0}'.format(cell)].value == '1':
            table.reg['M{0}'.format(cell)] = '12'
            bot.send_message(message.from_user.id, 'Показания 1?')
        if table.reg['J{0}'.format(cell)].value == '2':
            table.reg['M{0}'.format(cell)] = '11'
            bot.send_message(message.from_user.id, 'Тариф 2?')
        table.saving.save(table.Table)
    elif key == '11':
        table.reg['L{0}'.format(cell)] = message.text
        table.reg['M{0}'.format(cell)] = '12'
        bot.send_message(message.from_user.id, 'Показания 1?')
    elif key == '12':
        table.readings['{0}{1}'.format(functions.months.get(
            datetime.datetime.today().month - 1), cell + 1)] = message.text
        if table.reg['J{0}'.format(cell)].value == '1':
            table.reg['M{0}'.format(cell)] = ''
            bot.send_message(message.from_user.id, const.doneReg)
        if table.reg['J{0}'.format(cell)].value == '2':
            table.reg['M{0}'.format(cell)] = '13'
            bot.send_message(message.from_user.id, 'Показания 2?')
    elif key == '13':
        table.readings['{0}{1}'.format(functions.months.get(
            datetime.datetime.today().month - 1), cell + 2)] = message.text
        table.reg['M{0}'.format(cell)] = ''
        bot.send_message(message.from_user.id, const.doneReg )

    else:
        table.reg['{0}{1}'.format(keys_.get(key)[0], cell)] = message.text
        table.reg['M{0}'.format(cell)] = keys_.get(key)[1]
        table.saving.save(table.Table)
        bot.send_message(message.from_user.id, keys_.get(key)[2], parse_mode='HTML')

    table.saving.save(table.Table)
