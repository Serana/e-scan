import telebot
import const
import table
# import mail
import keyboards
import functions
import os
import notify
import datetime
from telebot.apihelper import ApiException
import requests
from detect import GetAnswer
from telebot import apihelper


bot = telebot.TeleBot(const.token)
apihelper.proxy = {'https': 'socks5://rkn:aufwidersehen@pofig.netangels.ru:8124',
                   'http': 'socks5://rkn:aufwidersehen@pofig.netangels.ru:8124'}
print('\n\nBot launch.\n\n')


def Notify():
    """ Проверка рассылки уведомлений """
    notify.CheckingNotify()
Notify()


try:
    @bot.message_handler(commands=['start'])
    def handle_text(message):
        if table.users(message.from_user.id)[0] == 'Yes':
            bot.send_message(message.from_user.id, const.itIsUser)
        else:
            cell = table.users(message.from_user.id)[1].index(None) + 2
            table.reg['A{0}'.format(cell)] = message.from_user.id
            table.notify['A{0}'.format(cell)] = message.from_user.id
            table.notify['C{0}'.format(cell)] = 'Уведомления отключены'
            table.saving.save(table.Table)
            bot.send_message(message.from_user.id, const.name, parse_mode='HTML',
                             reply_markup=keyboards.companiesKeyboard)


    @bot.message_handler(commands=['help'])
    def handle_text(message):
        """ Команда Help """
        if table.users(message.from_user.id)[0] == 'Yes':
            bot.send_message(message.from_user.id, const.help.format(functions.Date()),
                             reply_markup=keyboards.mainKeyboard, parse_mode='HTML')


    @bot.message_handler(content_types=['text'])
    def handle_text(message):
        cell = table.users(message.from_user.id)[1].index(message.from_user.id) + 2
        if table.users(message.from_user.id)[0] == 'Yes':
            try:
                cellA = table.Accounts().index(table.reg['C{0}'.format(cell)].value)
            except TypeError:
                cellA = table.Accounts_2().index(table.reg['C{0}'.format(cell)].value)

            if table.users(message.from_user.id)[0] == 'Yes':
                if table.reg['D{0}'.format(cell)].value == '1':
                    if int(message.text) in table.Accounts():
                        table.reg['C{0}'.format(cell)] = message.text
                        table.reg['D{0}'.format(cell)] = ''
                        table.saving.save(table.Table)
                        bot.send_message(message.from_user.id, const.doneReg.format(
                            functions.Name(message.from_user.id)), parse_mode='HTML',
                                         reply_markup=keyboards.mainKeyboard)
                    else:
                        table.reg['C{0}'.format(cell)] = message.text
                        table.reg['D{0}'.format(cell)] = ''
                        table.readings['A{0}'.format(table.Accounts_2().index(None) * 2 + 2)] = message.text
                        table.saving.save(table.Table)
                        bot.send_message(message.from_user.id, const.doneReg_, parse_mode='HTML',
                                         reply_markup=keyboards.mainKeyboard)

            if message.text == const.sendData:  # Отправить данные
                bot.send_message(message.from_user.id, const.dataMethod,
                                 reply_markup=keyboards.dataMethods)

            elif int(table.readings['N{0}'.format(cellA + 2)].value) == '1':
                table.readings['{0}{1}'.format(
                    functions.months.get(datetime.datetime.today().month),
                    cellA + 2)] = int(message.text)

                if functions.CounterMode(message.from_user.id) == '1':
                    table.readings['N{0}'.format(cellA + 2)] = ''

                    number = int(message.text)
                    month = functions.months.get(datetime.datetime.today().month)
                    cell = table.users(message.from_user.id)[1].index(message.from_user.id) + 2
                    table.readings['{0}{1}'.format(month, table.accountsList(cellA + 2) + 2)] = number
                    table.saving.save(table.Table)

                    lastMonth = table.readings['{0}{1}'.format(functions.months.get(
                        datetime.datetime.today().month - 1), table.accountsList(cellA + 2) + 2)].value
                    rate = int(table.reg['J{0}'.format(cell)].value)
                    difference = (int(number) - int(lastMonth))
                    price = difference * rate

                    text = const.accountData.format(number, lastMonth, price, rate)
                    bot.send_message(message.from_user.id, text, reply_markup=keyboards.sendMail,
                                     parse_mode='HTML')
                if functions.CounterMode(message.from_user.id) == '2':
                    table.readings['N{0}'.format(cellA + 2)] = '2'
                    bot.send_message(message.from_user.id, const.night2)
                table.saving.save(table.Table)

            elif table.readings['N{0}'.format(cellA + 2)].value == '2':
                table.readings['{0}{1}'.format(
                    functions.months.get(datetime.datetime.today().month),
                    cellA + 3)] = int(message.text)
                table.readings['N{0}'.format(cellA + 2)] = ''
                table.saving.save(table.Table)
                text = functions.TwoCounterData(cellA)
                bot.send_message(message.from_user.id, text, reply_markup=keyboards.sendMail,
                                 parse_mode='HTML')

            elif message.text == const.setNotify:
                answer = notify.Status(message.from_user.id)
                if answer == const.noNotify:
                    bot.send_message(message.from_user.id, const.noNotify,
                                     reply_markup=keyboards.notifyKeyboard)
                else:
                    bot.send_message(message.from_user.id, const.yesNotify.format(
                        table.notify['B{0}'.format(cell)].value), parse_mode='HTML',
                                     reply_markup=keyboards.noNotifyKeyboard)

            elif table.notify['D{0}'.format(cell)].value == 'Editing':
                table.notify['B{0}'.format(cell)] = message.text
                table.notify['D{0}'.format(cell)] = ''
                table.saving.save(table.Table)
                bot.send_message(message.from_user.id, const.doneSetNotify.format(
                    table.notify['B{0}'.format(cell)].value), parse_mode='HTML')

            elif message.text == const.donate:
                bot.send_message(message.from_user.id, const.donateText, reply_markup=keyboards.donateURL)

        if str(table.reg['D{0}'.format(cell)].value) == '1' and int(message.text) == ValueError:
            bot.send_message(message.from_user.id, 'Введите корректный лицевой счёт.')


    @bot.callback_query_handler(func=lambda call: True)
    def callback_inline(call):
        try:
            if table.users(call.from_user.id)[0] == 'Yes':
                cell = table.users(call.from_user.id)[1].index(call.from_user.id) + 2
                try:
                    cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)
                except TypeError:
                    cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)

                    # Отправка данных вручную
                if call.data == 'sendingByHand':
                    table.readings['N{0}'.format(cellA + 2)] = '1'
                    table.saving.save(table.Table)
                    bot.edit_message_text(text='Отправьте данные счётчика', chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML')
                # Отправка данных с фото
                if call.data == 'sendPhoto':
                    cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)
                    bot.delete_message(call.from_user.id, message_id=call.message.message_id)
                    table.readings['N{0}'.format(cellA + 2)] = '1'
                    if functions.CounterMode(call.from_user.id) == '1':
                        caption = const.sendPhoto
                    else:
                        caption = const.sendPhotoDay

                    table.saving.save(table.Table)
                    bot.send_photo(call.from_user.id, caption=caption,
                                   photo='http://elektroschyt.ru/wp-content/uploads/2014/08/E%60lektroschetchik-merkuriy-201.jpg')

                # Корректировка данных
                elif call.data == 'EditData':
                    bot.edit_message_text(text=const.editingData, chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML',
                                          reply_markup=keyboards.editingData)
                elif call.data == 'byHand':
                    cellA = table.AccountsList().index(int(table.reg['C{0}'.format(cell)].value))
                    table.readings['N{0}'.format(cellA + 2)] = '1'
                    table.saving.save(table.Table)
                    if functions.CounterMode(call.from_user.id) == '1':
                        bot.edit_message_text(text='Напишите данные повторно', chat_id=call.from_user.id,
                                              message_id=call.message.message_id, parse_mode='HTML')
                    if functions.CounterMode(call.from_user.id) == '2':
                        bot.edit_message_text(text='Отправьте данные показаний счётчика (дневной тариф)',
                                              chat_id=call.from_user.id,
                                              message_id=call.message.message_id, parse_mode='HTML')

                if call.data == 'SendDataMail':
                    table.ticket['D2'] = '{0} {1}'.format(functions.LastName(call.from_user.id),
                                                          functions.Name(call.from_user.id))
                    table.ticket['D14'] = '{0} {1}'.format(functions.LastName(call.from_user.id),
                                                           functions.Name(call.from_user.id))
                    table.ticket['E7'] = table.readings['{0}{1}'.format(functions.months.get(
                        datetime.datetime.today().month), cellA + 2)].value
                    table.ticket['E19'] = table.readings['{0}{1}'.format(functions.months.get(
                        datetime.datetime.today().month), cellA + 2)].value

                    table.ticket['E8'] = table.readings['{0}{1}'.format(functions.months.get(
                        datetime.datetime.today().month - 1), cellA + 2)].value
                    table.ticket['E20'] = table.readings['{0}{1}'.format(functions.months.get(
                        datetime.datetime.today().month - 1), cellA + 2)].value

                    table.saving_3.save(table.Table_3)

                    # mail.SendMail(call.from_user.id)
                    bot.edit_message_text(text=const.doneSendMail, chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML')

                if call.data == 'UK_1' or call.data == 'UK_2' or call.data == 'UK_3':
                    table.reg['B{0}'.format(cell)] = functions.ucompanies.get(call.data)
                    table.reg['D{0}'.format(cell)] = '1'
                    table.saving.save(table.Table)
                    bot.edit_message_text(text='Введите номер Вашего лицевого счета', chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML')

                elif call.data == '1_rate' or call.data == '2_rate':
                    table.reg['J{0}'.format(cell)] = str(call.data)[0]
                    table.reg['M{0}'.format(cell)] = '10'
                    table.saving.save(table.Table)
                    bot.edit_message_text(text=const.rate, chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML')
                elif call.data == 'photoAgain':
                    cellA = table.AccountsList().index(int(table.reg['C{0}'.format(cell)].value))
                    bot.delete_message(call.from_user.id, message_id=call.message.message_id)
                    table.readings['N{0}'.format(cellA + 2)] = '1'
                    table.saving.save(table.Table)
                    if functions.CounterMode(call.from_user.id) == '1':
                        caption = const.sendPhoto
                    else:
                        caption = const.sendPhotoDay
                    bot.send_photo(call.from_user.id, caption=caption,
                                   photo='https://images.zakupka.com/i/firms/27/13/13443/elektroschetchik-leb-d1-o5-energo-kontrol_d794734501c2fb2_300x300_1.jpg')

                elif call.data == 'cancel':
                    bot.delete_message(call.from_user.id, message_id=call.message.message_id)
                    bot.send_message(call.from_user.id, 'Главное меню',
                                     reply_markup=keyboards.mainKeyboard)

                elif call.data == 'setNotify':
                    table.notify['D{0}'.format(cell)] = 'Editing'
                    table.notify['C{0}'.format(cell)] = ''
                    table.saving.save(table.Table)
                    bot.edit_message_text(text=const.notifyNumber, chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML')

                elif call.data == 'offNotify':
                    table.notify['B{0}'.format(cell)].value = ''
                    table.notify['C{0}'.format(cell)].value = 'Уведомления отключены'
                    table.saving.save(table.Table)
                    bot.edit_message_text(text='Уведомления отключены.', chat_id=call.from_user.id,
                                          message_id=call.message.message_id, parse_mode='HTML')

        except ValueError as error:
            bot.send_message(call.from_user.id, 'Произошла ошибка.\n{0}'.format(error))


    @bot.message_handler(content_types=['photo'])
    def handle_photo(message):
        if table.users(message.from_user.id)[0] == 'Yes':

            cell = table.users(message.from_user.id)[1].index(message.from_user.id) + 2
            try:
                cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)
            except ValueError:
                cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)
            except TypeError:
                cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)

            bot.send_message(message.from_user.id, '\U0001F50D Идёт распознавание...')

            # Распознавание
            path = bot.get_file(message.photo[2].file_id).file_path  # file_path
            photoURL = 'https://api.telegram.org/file/bot{0}/{1}'.format(const.token, path)  # URL фотографии
            f = open('01.jpg', 'wb')
            print('photoURL:', photoURL)
            request = requests.get(photoURL, proxies=apihelper.proxy)
            f.write(request.content)
            e, idd, imgs = GetAnswer(request.content)
            #print('neyro', e, idd, imgs)
            img = open('ImagesC/' + str(idd) + '-1.jpg', 'rb')
            f.close()
            img.close()
            number = int('{0}{1}{2}{3}'.format(e[1], e[2], e[3], e[4]))
            print('number', number)
            if table.readings['N{0}'.format(cellA + 2)].value == '1':
                if functions.CounterMode(message.from_user.id) == '1':
                    print("table.AccountsList()", table.AccountsList())
                    print(int(table.reg['C{0}'.format(cell)].value))
                    cellA = table.AccountsList().index(table.reg['C{0}'.format(cell)].value)
                    month = functions.months.get(datetime.datetime.today().month)
                    table.readings['{0}{1}'.format(month, cellA + 2)] = number
                    #table.readings['N{0}'.format(cellA + 2)] = ''
                    #table.reg['J{0}'.format(cellA + 2)] = ''
                    table.saving.save(table.Table)
                    lastMonth = table.readings['{0}{1}'.format(functions.months.get(
                        datetime.datetime.today().month - 1), cellA + 2)].value
                    rate = 3.2
                    if not lastMonth:
                        lastMonth = 0
                    difference = (int(number) - int(lastMonth))
                    price = difference * rate
                    print('number', number)
                    print('price', price)
                    text = const.accountData.format(number, lastMonth, round(price, 2), rate)
                    #bot.send_message(message.from_user.id, 'Введите корректный лицевой счёт.')
                    bot.edit_message_text(text, message.from_user.id, message.message_id + 1,
                                          reply_markup=keyboards.sendMail, parse_mode='HTML')
                if functions.CounterMode(message.from_user.id) == '2':
                    number = 2018

                    month = functions.months.get(datetime.datetime.today().month)
                    table.readings['{0}{1}'.format(month, cellA + 2)] = number

                    table.readings['N{0}'.format(cellA + 2)] = '2'
                    table.saving.save(table.Table)
                    print('number', number)
                    print('price', price)
                    bot.send_message(message.from_user.id, 'Введите корректный лицевой счёт.1')
                    bot.send_message(message.from_user.id, const.night)
                #else:
                 #   print('ne zashel')
                  #  bot.send_message(message.from_user.id, 'У нас проблемы...')
            elif table.readings['N{0}'.format(cellA + 2)].value == '2':
                month = functions.months.get(datetime.datetime.today().month)
                table.readings['{0}{1}'.format(month, cellA + 3)] = number

                table.readings['N{0}'.format(cellA + 2)] = ''
                table.saving.save(table.Table)
                print('number', number)
                text = functions.TwoCounterData(cellA)
                bot.send_message(message.from_user.id, 'Введите корректный лицевой счёт.3')
                bot.send_message(message.from_user.id, text, reply_markup=keyboards.sendMail,
                                 parse_mode='HTML')
            else:
                rate = 3.2
                price = number * rate
                print('number', number)
                print('price', price)
                text = const.justPhoto.format(number, round(price, 2), rate)
                bot.send_message(message.from_user.id, 'Введите корректный лицевой счёт.3')
                bot.edit_message_text(text, message.from_user.id, message.message_id + 1,
                                      parse_mode='HTML')

        else:
            bot.send_message(message.from_user.id, '\U0001F50D Идёт распознавание...')

            path = bot.get_file(message.photo[2].file_id).file_path  # file_path
            photoURL = 'https://api.telegram.org/file/bot{0}/{1}'.format(const.token, path)  # URL фотографии
            f = open('01.jpg', 'wb')
            print('photoURL:', photoURL)
            request = requests.get(photoURL, proxies=apihelper.proxy)
            f.write(request.content)
            e, idd, imgs = GetAnswer(request.content)
            img = open('ImagesC/' + str(idd) + '-1.jpg', 'rb')
            f.close()
            img.close()

            number = int('{0}{1}{2}{3}'.format(e[1], e[2], e[3], e[4]))
            rate = 3.2
            price = number * rate
            print('number', number)
            print('price', price)
            bot.edit_message_text(const.noAccountData.format(number, round(price, 2), rate),
                                  message.from_user.id, message.message_id + 1,
                                  parse_mode='HTML')
            bot.send_message(message.from_user.id, 'Введите корректный лицевой счёт.')
    bot.polling(none_stop=True)

except ApiException as err:
    print(err)
    os.system('main.py')
except ValueError as err:
    os.system('main.py')
except ZeroDivisionError as err:
    os.system('main.py')
else:
    print('else')
    os.system('main.py')