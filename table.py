import openpyxl

# Data.xlsx
Table = 'Data.xlsx'
saving = openpyxl.Workbook()
saving = openpyxl.load_workbook(Table)
#reg = saving.get_sheet_by_name('Reg')
reg = saving['Регистрация']
#readings = saving.get_sheet_by_name('Readings')
readings=saving['Показания']
#notify = saving.get_sheet_by_name('Notify')
notify=saving['Напоминания']
saving.save(Table)


Table_3 = 'Квитанция.xlsx'
saving_3 = openpyxl.Workbook()
saving_3 = openpyxl.load_workbook(Table_3)
#ticket = saving_3.get_sheet_by_name('Бланк для оплаты в МЭС')
ticket = saving_3['Бланк для оплаты в МЭС']
saving_3.save(Table_3)

notifyList = [v[0].value for v in notify.iter_rows('B2:B100')]


def users(UserID):
    usersList = [v[0].value for v in reg.iter_rows('A2:A100')]  # ID пользователей
    if UserID in usersList:
        print('yes')
        return ['Yes', usersList]
    else:
        print('no')
        return ['No', usersList]

def AccountsList():
    return [v[0].value for v in readings.iter_rows('A2:A200')]


def UsersList():
    Users = [v[0].value for v in reg.iter_rows('A2:A200')]
    return Users


def Names():
    acc = [v[0].value for v in readings.iter_rows('O2:O200')]
    return acc


def Accounts():
    acc = [v[0].value for v in readings.iter_rows('A2:A200')]
    return acc


def Accounts_2():
    acc = []
    i = 2
    while i < 200:
        acc.append(readings['A{0}'.format(i)].value)
        i += 2
    return acc


def accountsList(cell):
    accounts_List = [v[0].value for v in readings.iter_rows('A2:A200')]
    try:
        return accounts_List.index(reg['C{0}'.format(cell)].value)
    except TypeError:
        return accounts_List.index(reg['C{0}'.format(cell)].value)
