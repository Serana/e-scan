try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# If you don't have tesseract executable in your PATH, include the following:
pytesseract.pytesseract.tesseract_cmd = r'C:\Users\User\Desktop\EScanBot\e-scan\venv\Lib\site-packages\pytesseract-0.2.5-py3.7.egg-info'
# Example tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract\'

# Simple image to string
print(pytesseract.image_to_string(Image.open(r'C:\Users\User\Desktop\EScanBot\e-scan\test.jpg')))
